import pydantic


class BonusSkillPoints(pydantic.BaseModel):
    initial: int
    level_up: int
