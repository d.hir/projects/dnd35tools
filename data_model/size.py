import pydantic
import enum


class Shape(str, enum.Enum):
    Tall = 'tall'
    Long = 'long'


class Size(pydantic.BaseModel):
    id: str
    basic_name: str
    shape: Shape
    offset: int
    att_ac_modifier: int
    special_att_modifier: int
    hide_modifier: int
    space: float
    reach: int
