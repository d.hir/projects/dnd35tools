import pydantic

from .language import Language
from .skill import BonusSkillPoints
from .feat import BonusFeats
from .ability import Abilities


class Race(pydantic.BaseModel):
    id: str
    name: str
    ability_adjustments: Abilities
    size: str
    bonus_languages: list[Language]
    bonus_skill_points: BonusSkillPoints
    special_abilities: list[str]
    special_qualities: list[str]
    bonus_feats: BonusFeats
    natural_armor: int
    level_adjustment: int
