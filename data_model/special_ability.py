import pydantic


class SpecialAbility(pydantic.BaseModel):
    id: str
    name: str
    description: str
