import pydantic


class Abilities(pydantic.BaseModel):
    Str: int
    Dex: int
    Con: int
    Int: int
    Wis: int
    Cha: int
