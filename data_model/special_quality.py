import pydantic


class SpecialQuality(pydantic.BaseModel):
    id: str
    name: str
    description: str
