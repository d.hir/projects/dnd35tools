import pydantic


class Feat(pydantic.BaseModel):
    id: str
    name: str
    description: str


class BonusFeats(pydantic.BaseModel):
    inherent: list[str]
    initial: int
    level_up: int
