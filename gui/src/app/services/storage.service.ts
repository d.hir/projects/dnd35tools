import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Size } from '../interfaces/dnd/size';
import { HttpInterface, DataInterface } from '../util/storage/data-interface';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  PORT = 8000;
  ADDRESS = '127.0.0.1';

  URL = `http://${this.ADDRESS}:${this.PORT}`

  constructor(private http: HttpClient) { }

  sizes: DataInterface<Size> = new HttpInterface(this.URL + '/templates/sizes', this.http);
}
