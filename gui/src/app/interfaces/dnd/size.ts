import { DisplayName } from "src/app/body/templates/template-table/template-table.component";
import { Interfacable } from "src/app/util/storage/data-interface";


export enum Shape {
    Tall = "tall",
    Long = "long"
}


export interface Size extends Interfacable {
    name: string;
    shape: Shape;
    offset: number;
    att_ac_modifier: number;
    special_att_modifier: number;
    hide_modifier: number;
    space: number;
    reach: number;
}


function SizeID(s: Size): string {
    return s.id;
}


export const SizeValueRestrictions: Map<string, any[]> = new Map<string, any[]>([
    ['shape', ['tall', 'long']]
]);


export const SizeDisplayNames: Map<string, DisplayName> = new Map<string, DisplayName>([
    ['id', { title: 'ID', description: 'Unique ID' }],
    ['basic_name', { title: 'Name', description: 'Basic shape independent name' }],
    ['shape', { title: 'Shape', description: 'Shape (either tall or long)' }],
    ['offset', { title: 'Offset', description: 'Offset from core/medium size' }],
    ['att_ac_modifier', { title: 'Att/AC Modifier', description: 'Attack and AC Modifier' }],
    ['special_att_modifier', { title: 'Sp. Att. Modifier', description: 'Special Attack Modifier' }],
    ['hide_modifier', { title: 'Hide Modifier', description: 'Modifier to Hide Skill' }],
    ['space', { title: 'Space', description: 'Space taken up by creature (in ft)' }],
    ['reach', { title: 'Reach', description: 'Reach of the creature (in ft)' }]
]);
