export interface InternalLink {
    name: string;
    target: string;
}
