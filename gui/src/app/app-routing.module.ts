import { NgModule } from '@angular/core';
import { RouterModule, Routes, TitleStrategy } from '@angular/router';

import { ExtendedTitleStrategy } from './title-strategy';

import { TemplatesComponent } from './body/templates/templates.component';
import { SizesComponent } from './body/templates/sizes/sizes.component';

const routes: Routes = [
  {
    path: 'templates',
    component: TemplatesComponent,
    children: [
      {
        path: 'sizes',
        component: SizesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: TitleStrategy, useClass: ExtendedTitleStrategy }
  ]
})
export class AppRoutingModule { }
