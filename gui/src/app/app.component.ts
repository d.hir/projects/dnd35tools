import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { RibbonComponent } from './ribbon/ribbon.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    standalone: true,
    imports: [RibbonComponent, RouterOutlet]
})
export class AppComponent {
  title = 'dnd35gui';
}
