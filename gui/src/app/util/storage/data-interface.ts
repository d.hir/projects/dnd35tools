import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";


export interface Interfacable {
    id: string;
}


export abstract class DataInterface<T extends Interfacable> {
    abstract get(): Observable<T[]>;
    abstract get(identifier: string): Observable<T>;
    abstract post(payload: T): void;
    abstract post(payload: T, identifier: string): void;
    abstract put(payload: T, identifier: string): void;
}


export class HttpInterface<T extends Interfacable> implements DataInterface<T> {
    url: string;

    constructor(url: string, private http: HttpClient) {
        this.url = url + '/';
    }

    get(): Observable<T[]>;
    get(identifier: string): Observable<T>;
    get(identifier?: string): Observable<T> | Observable<T[]> {
        const url = identifier === undefined ? this.url : this.url + identifier + '/';
        return this.http.get<T>(url);
    }

    post(payload: T): void;
    post(payload: T, identifier: string): void;
    post(payload: T, identifier?: string): void {
        identifier = identifier === undefined ? identifier : payload.id;
        this.http.post<T>(this.url + identifier + '/', payload);
    }

    put(payload: T, identifier?: string): void {
        identifier = identifier === undefined ? identifier : payload.id;
        this.http.put<T>(this.url + identifier + '/', payload);
    }
}
