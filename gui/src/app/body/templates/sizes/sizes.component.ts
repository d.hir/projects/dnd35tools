import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateTableComponent, DisplayName } from '../template-table/template-table.component';

import { SizeDisplayNames, SizeValueRestrictions } from 'src/app/interfaces/dnd/size';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-sizes',
  standalone: true,
  imports: [CommonModule, TemplateTableComponent],
  templateUrl: './sizes.component.html',
  styleUrls: ['./sizes.component.scss']
})
export class SizesComponent {
  restrictValues: Map<string, any[]> = SizeValueRestrictions;
  prettyNames: Map<string, DisplayName> = SizeDisplayNames;

  constructor(public storage_service: StorageService) {}
}
