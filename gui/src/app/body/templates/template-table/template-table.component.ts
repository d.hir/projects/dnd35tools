import { Component, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MatTableModule, MatTable, MatTableDataSource } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { Observable, Subscription } from 'rxjs';
import { DataInterface, Interfacable } from 'src/app/util/storage/data-interface';


class DataEditorConfig<T> {
  public restrictedValues?: T[];
  public value?: T;
  public key: string;

  constructor(key: string, value?: T) {
    this.key = key;
    this.value = value;
  }

  isRestricted(): boolean {
    return this.restrictedValues !== undefined;
  }
}


export interface DisplayName {
  title: string;
  description: string;
}


@Component({
  selector: 'app-template-table',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    MatButtonModule,
    MatIconModule
  ],
  templateUrl: './template-table.component.html',
  styleUrls: ['./template-table.component.scss']
})
export class TemplateTableComponent<Data extends Interfacable> implements OnInit, OnDestroy {
  @Input({ required: true }) dataInterface!: DataInterface<Data>;
  @Input() restrictValues?: Map<string, any[]>;
  @Input() prettyNames?: Map<string, DisplayName>;
  
  @ViewChild(MatTable, { static: true }) table?: MatTable<Data>;
  @ViewChild(MatSort, { static: true }) sort?: MatSort;
  
  dataSource: MatTableDataSource<Data> = new MatTableDataSource<Data>();
  private dataObservable?: Observable<Data[]>;
  columnNames: string[] = [];
  selectedRow?: Data;
  private dataObservableSubscription?: Subscription;

  editorConfigs: DataEditorConfig<any>[] = [];

  ngOnInit(): void {
    this.dataObservable = this.dataInterface.get();
    this.dataObservableSubscription = this.dataObservable!.subscribe((d: Data[]) => {
      this.dataSource!.data = d;
      this.dataSource!.sort = this.sort!;
      this.columnNames = Object.keys(d[0] as Object);
      this.table?.renderRows();
      this.setEditorConfig(d[0], false);
    })
  }

  ngOnDestroy(): void {
    this.dataObservableSubscription?.unsubscribe();
  }

  setEditorConfig(fromRow: Data, includeValues: boolean): void {
    this.editorConfigs = [];
    const entries = Object.entries(fromRow as Object);
    const entryMap = new Map<string, any>(entries);
    for (const [key, value] of entryMap) {
      const values = this.restrictValues?.get(key);
      const usedValue = includeValues ? value : undefined;
      let newEntry = new DataEditorConfig<typeof value>(key, usedValue);
      if (values !== undefined) {
        newEntry.restrictedValues = values;
      }
      this.editorConfigs.push(newEntry);
    }
  }

  fromEditorConfig(): Data {
    const data = this.editorConfigs.map((editorConfig: DataEditorConfig<Data>) => [editorConfig.key, editorConfig.value]);
    const d: Data = Object.fromEntries(data);
    return d;
  }

  selectRow(row: Data): void {
    if (this.selectedRow == row) {
      this.selectedRow = undefined;
      this.setEditorConfig(row, false);
    } else {
      this.selectedRow = row;
      this.setEditorConfig(this.selectedRow, true);
    }
  }

  isRowSelected(row: Data): boolean {
    return row === this.selectedRow;
  }

  title(name: string): string {
    const pretty = this.prettyNames?.get(name);
    return pretty === undefined ? name : pretty.title;
  }

  description(name: string): string | undefined {
    const pretty = this.prettyNames?.get(name);
    return pretty === undefined ? name : pretty.description;
  }

  getUpdateIcon(): string {
    if (this.selectedRow === undefined) {
      return 'add';
    } else {
      return 'sync_alt';
    }
  }

  syncData(): void {
    const d: Data = this.fromEditorConfig();
    if (this.selectedRow === undefined) {
      this.dataInterface.post(d)
    }
  }
}
