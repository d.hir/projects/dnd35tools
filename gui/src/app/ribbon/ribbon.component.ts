import { Component } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

import { RibbonMenuComponent } from './ribbon-menu/ribbon-menu.component';

import { InternalLink } from '../interfaces/util/internal-link';

@Component({
    selector: 'app-ribbon',
    imports: [ RibbonMenuComponent, RouterModule ],
    templateUrl: './ribbon.component.html',
    styleUrls: ['./ribbon.component.scss'],
    standalone: true
})
export class RibbonComponent {
  template_items: InternalLink[] = [
    { name: "Sizes", target: "/templates/sizes" },
    { name: "Races", target: "/templates/races" },
  ];
}
