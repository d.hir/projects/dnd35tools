import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';

import { InternalLink } from 'src/app/interfaces/util/internal-link';

@Component({
  selector: 'app-ribbon-menu',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatMenuModule, RouterModule],
  templateUrl: './ribbon-menu.component.html',
  styleUrls: ['./ribbon-menu.component.scss']
})
export class RibbonMenuComponent {
  @Input() items: InternalLink[] = [];
}
