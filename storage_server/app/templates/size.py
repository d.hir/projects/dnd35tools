from ..util.storage import StorageInterface, Endpoints

from data_model import size


interface = StorageInterface[size.Size]('sizes', disabled_endpoints={Endpoints.get})


@interface.router.get('/{name}')
async def get_size(name: str, shape: size.Shape = size.Shape.Tall):
    shape = shape
    basic_name = name
    split = name.split('_')
    if len(split) > 1:
        try:
            shape = size.Shape(split[-1])
            basic_name = '_'.join(split[:-1])
        except ValueError:
            pass
    return await interface.get(f'{basic_name}_{shape.value}')


# @interface.router.post('/')
# async def create_size(size: size.Size, identifier: str):
#     if 
#     shape = shape
#     basic_name = name
#     split = name.split('_')
#     if len(split) > 1:
#         try:
#             shape = size.Shape(split[-1])
#             basic_name = '_'.join(split[:-1])
#         except ValueError:
#             pass
#     return await interface.get(f'{basic_name}_{shape.value}')
