import fastapi

from . import size
from . import race


router = fastapi.APIRouter(
    prefix='/templates'
)

router.include_router(size.interface.router)
router.include_router(race.interface.router)
