from ..util.storage import StorageInterface

from data_model import race


interface = StorageInterface[race.Race]('races')
