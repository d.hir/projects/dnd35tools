import json
import pathlib
import fastapi
import enum
import functools
import typing
import dataclasses
import pydantic


DATA_ROOT = pathlib.Path(__file__).parent / '..' / '..' / '..' / 'data'
TEMPLATE_ROOT = DATA_ROOT / 'templates'
TEMPLATE_NAMESPACES = [pathlib.Path(nsdir.name) for nsdir in TEMPLATE_ROOT.iterdir()]


def _absolutify(path: pathlib.Path | str) -> pathlib.Path:
    path = pathlib.Path(path)
    if not path.is_absolute():
        path = DATA_ROOT / path
    if not DATA_ROOT in path.parents:
        raise PermissionError('Cannot store outside of root data storage directory')
    return path


def load_file(path: pathlib.Path | str) -> dict:
    path = _absolutify(path)
    with open(path, 'r') as f:
        data = json.load(f)
    return data


def load_dir(path: pathlib.Path | str) -> list[dict]:
    path = _absolutify(path)
    return [load_file(d) for d in path.iterdir()]


def store(path: pathlib.Path | str, data: dict) -> None:
    path = _absolutify(path)
    pathlib.Path.mkdir(path.parent, parents=True, exist_ok=True)
    with open(path, 'w+') as f:
        json.dump(data, f, indent=4)


T = typing.TypeVar('T', bound=pydantic.BaseModel)


class StorageInterface(typing.Generic[T]):
    def __init__(self: 'StorageInterface', category: str, disabled_endpoints: set['StorageInterface.Endpoints'] = set()) -> None:
        self.category = category
        self.router = fastapi.APIRouter(
            prefix = '/' + category
        )

        for endpoint in Endpoints:
            if endpoint not in disabled_endpoints:
                self.router.add_api_route(endpoint.value.route, endpoint.bind(self), methods=[endpoint.value.variant])

    async def get_all(self: 'StorageInterface') -> list[T]:
        for namespace in TEMPLATE_NAMESPACES:
            path = _absolutify(TEMPLATE_ROOT / namespace / self.category)
            try:
                l = []
                for file in path.iterdir():
                    with open(file, 'r') as f:
                        data = f.read()
                    l.append(self.__orig_class__.__args__[0].__pydantic_validator__.validate_json(data))
                return l
            except FileNotFoundError:
                pass
        raise fastapi.HTTPException(status_code=404, detail=f'Directory containing {self.category} not found')

    def _get(self: 'StorageInterface', namespace: pathlib.Path, path: pathlib.Path) -> typing.Optional[T]:
        path = _absolutify(TEMPLATE_ROOT / namespace / self.category / path)
        try:
            with open(path, 'r') as f:
                data = f.read()
            return self.__orig_class__.__args__[0].__pydantic_validator__.validate_json(data)
        except FileNotFoundError:
            pass
        return None

    @staticmethod
    def _split_id(identifier: str) -> tuple[typing.Optional[str], pathlib.Path]:
        identifier = identifier.lower()
        try:
            namespace, pathstring = identifier.split('/', 1)
            path = pathlib.Path(pathstring)
            path = path.parent / f'{path.name}.json'
            return namespace, path
        except ValueError:
            return None, pathlib.Path(identifier)

    async def get(self: 'StorageInterface', identifier: str) -> T:
        ns_str, path = self._split_id(identifier)
        ns_str = ns_str if ns_str is not None else 'core'
        ns = pathlib.Path(ns_str)
        obj = self._get(ns, path)
        if obj is not None:
            return obj
        path = ns / path
        for namespace in TEMPLATE_NAMESPACES:
            self._get(namespace, path)
        raise fastapi.HTTPException(status_code=404, detail=f'{self.category} {identifier} not found')

    async def post(self: 'StorageInterface', data: T, identifier: str) -> None:
        namespace, path = self._split_id(identifier)
        namespace = namespace if namespace is not None else 'user'
        path = TEMPLATE_ROOT / namespace / self.category / path
        store(path, data.model_dump())


@dataclasses.dataclass
class EndpointData:
    callback: typing.Callable
    route: str
    variant: str


class Endpoints(enum.Enum):
    get_all = EndpointData(StorageInterface.get_all, '/', 'GET')
    get = EndpointData(StorageInterface.get, '/{name}', 'GET')
    post = EndpointData(StorageInterface.post, '/', 'POST')

    def bind(self, *args, **kwargs):
        return functools.partial(self.value.callback, *args, **kwargs)
